# Exodus App

Author: Rodrigo Antinarelli

# Instructions

- Clone the repository `git clone https://github.com/jprichardson/rodrigo-portfolio`
- Install dependencies `yarn install`
- Run the project `yarn start`

