import axios from 'axios'

export const API_URL = 'https://api.coinmarketcap.com/v2'
export const LIST_URL = `${API_URL}/listings/`
export const CRYPTO_DATA = `${API_URL}/ticker/`

const listCryptocurrencies = () => {
  return axios.get(LIST_URL)
    .then(res => {
      return res.data.data
    })
    .catch(function (error) {
      console.log(error.message)
    })
}

const getCryptoData = (id) => {
  return axios.get(`${CRYPTO_DATA}${id}/`)
    .then(res => {
      return res.data.data
    })
    .catch(function (error) {
      console.log(error.message)
    })
}

export {
  listCryptocurrencies,
  getCryptoData
}
