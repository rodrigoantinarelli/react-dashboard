import React from 'react'
import { Route, Switch } from 'react-router-dom'
import { Home, Portfolio, NoMatch } from '@/Containers'

const routes = [
  { path: '/', exact: true, component: Home },
  { path: '/portfolio', component: Portfolio },
  { component: NoMatch }
]

const renderRoutes = () => {
  return (
    <Switch>
      {routes.map((route, i) => {
        return (
          <Route
            key={i}
            path={route.path}
            exact={route.exact}
            strict={route.strict}
            render={props => <route.component {...props} route={route} />}
          />
        )
      })}
    </Switch>
  )
}

export default renderRoutes
