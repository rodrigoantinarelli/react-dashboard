import styled from 'styled-components'
import { Colors } from '@/Resources/Styles/Theme'

export const ChartArea = styled.div`
  text-align: center;
  margin-bottom: 10px;
  width: 940px;
  margin: 0 auto;
  position: relative;

  .tooltip {

    &__line-area {
      opacity: 0;
      transition: all 300ms ease;
    }

    &__vertical-line {
      stroke: rgba(255, 255, 255, 0.5);
      transition: all 300ms ease;
    }
  }

  .label-title {
    font-size: 60px;
    letter-spacing: -1.7px;
    fill: ${Colors.white.color};

    &__small-letter {
      font-size: 42px;
    }
  }

  .label-subtitle {
    font-size: 24px;
    letter-spacing: -0.7px;
    fill: ${Colors.gray.color};
  }

`
export const ChartTooltip = styled.div`
  position: absolute;
  text-align: center;
  min-width: 206px;
  padding: 10px 20px;
  border-radius: 5px;
  pointer-events: none;
  color: white;
  font-size: 20px;
  background-color: rgba(43, 49, 55, 0.9);
  box-shadow: 0 50px 60px 0 #00000080;
  opacity: 0;
  transition: all 300ms ease;

  &:after {
    content: '';
    width: 0;
    height: 0;
    border-style: solid;
    border-width: 10px 10px 0 10px;
    border-color: rgba(43, 49, 55, 0.9) transparent transparent transparent;
    position: absolute;
    top: 100%;
    left: 50%;
    transform: translateX(-50%);
  }

  .tooltip {

    &__title {
      font-size: 16px;
      letter-spacing: -0.2px;
      color: #ffffff;
      display: block;

      small {
        font-size: 12px;
      }
    }

    &__subtitle {
      opacity: 0.6;
      font-size: 14px;
      letter-spacing: -0.2px;
      color: #ffffff;
      display: block;
    }
  }
`
