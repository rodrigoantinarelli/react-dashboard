import * as d3 from 'd3'
import DATA from '@/Data/lineChartData.json'
import ASSETS from '@/Data/coins.json'
import moment from 'moment'

const createLineChart = () => {

  const width = 940
  const height = 410
  const margin = { top: 30, right: 0, bottom: 30, left: 0 }
  const sizeX = width - margin.left - margin.right
  const sizeY = height - margin.top - margin.bottom

  const TOTAL_ASSETS = ASSETS.length

  const svg = d3.select('#line-chart-area')
    .append('svg')
    .attr('width', width)
    .attr('height', height)

  const g = svg.append('g')
    .attr('transform',  `translate(${margin.left},${margin.top})`)

  const x = d3.scaleTime().rangeRound([0, sizeX])
  const y = d3.scaleLinear().rangeRound([sizeY, margin.top + margin.bottom])

  const line = d3.line()
    .x((d) => x(new Date(d.date)))
    .y((d) => y(parseInt(d.amount, 10)))

  x.domain(d3.extent(DATA, (d) => { return new Date(d.date) }))
  y.domain(d3.extent(DATA, (d) => { return parseInt(d.amount, 10) }))

  const defs = svg.append('defs')

  const gradient = defs.append('linearGradient')
    .attr('id', 'svgGradient')
    .attr('x1', '0%')
    .attr('x2', '100%')
    .attr('y1', '0%')
    .attr('y2', '100%')

  gradient.append('stop')
    .attr('class', 'start')
    .attr('offset', '0%')
    .attr('stop-color', '#f7618b')
    .attr('stop-opacity', 1)

  gradient.append('stop')
    .attr('class', 'end')
    .attr('offset', '100%')
    .attr('stop-color', '#2a7aff')
    .attr('stop-opacity', 1)

  gradient.append('stop').attr('stop-color', 'black')
  gradient.append('stop').attr('offset', '100%').attr('stop-color', 'magenta')

  const path = g.append('path')

  path.datum(DATA)
    .attr('fill', 'none')
    .attr('stroke', 'url(#svgGradient)')
    .attr('stroke-linejoin', 'round')
    .attr('stroke-linecap', 'round')
    .attr('stroke-width', 3)
    .attr('d', line)

  const totalLength = path.node().getTotalLength()

  path
    .attr("stroke-dasharray", totalLength + " " + totalLength)
    .attr("stroke-dashoffset", totalLength)
    .transition()
    .duration(1000)
    .ease(d3.easeLinear)
    .attr("stroke-dashoffset", 0)

  const tooltipLineArea = g.append("g").attr('class', 'tooltip__line-area')

  const tooltipBox = d3.select('#line-chart-tooltip')
    .style('opacity', 0)


  tooltipLineArea.append("line")
    .attr("class", "tooltip__vertical-line")
    .attr("y1", 0)
    .attr("y2", height)


  const showTooltipBox = () => {
    return tooltipBox.transition().style('opacity', 1)
  }

  const showTooltipArea = () => {
    return tooltipLineArea.transition().style('opacity', 1)
  }

  const showTooltip = () => {
    showTooltipBox()
    showTooltipArea()
  }

  const hideTooltipBox = () => {
    return tooltipBox.transition().style('opacity', 0)
  }

  const hideTooltipArea = () => {
    return tooltipLineArea.transition().style('opacity', 0)
  }

  const hideTooltip = () => {
    hideTooltipBox()
    hideTooltipArea()
  }

  const bisectDate = d3.bisector((d) => { return new Date(d.date) }).left

  function moveTooltip () {
    const mouse = d3.mouse(this)
    const trackDate = x.invert(mouse[0])
    const i = bisectDate(DATA, trackDate, 1)
    const previousDate = DATA[i - 1]
    const nextDate = DATA[i]
    const d = trackDate - new Date(previousDate.date) > new Date(nextDate.date) - trackDate ? nextDate : previousDate
    const date = new Date(d.date)
    const formattedDate = moment(date).format('dddd, MMM M, YYYY, HH:mm:ss')
    const amount = parseInt(d.amount, 10)

    tooltipLineArea.select(".tooltip__vertical-line")
      .attr("transform", "translate(" + x(date) + "," + y(amount) + ")")
      .attr("y2", height - y(amount))

    tooltipBox
      .style('left', `${x(date)}px`)
      .style('top', `${y(amount)}px`)
      .style('margin-left', `-${tooltipBox.node().getBoundingClientRect().width / 2}px`)
      .style('margin-top', `-${tooltipBox.node().getBoundingClientRect().height / 2 + 10}px`)
      .html(`
        <span class='tooltip__title'><small>$</small>${d.amount}</span>
        <span class='tooltip__subtitle'>${formattedDate}</span>
      `)
  }

  g.append("rect")
    .attr("width", width)
    .attr("height", height)
    .style("fill", "none")
    .style("pointer-events", "all")
    .on("mouseover", () => showTooltip() )
    .on("mouseout", () => hideTooltip() )
    .on("mousemove", moveTooltip)

  const chartLabel = svg.append('g')
    .attr('class', 'chart-label-group')
    .attr('transform',  `translate(${width/2}, ${margin.top})`)

  chartLabel
    .append('text')
    .attr('id', 'label-title')
    .attr('class', 'label-title')
    .attr('text-anchor', 'middle')
    .attr('dy', '-.8em')
    .style('opacity', 0)
    .transition()
    .ease(d3.easeLinear)
    .duration(500)
    .style('opacity', 1)
    .attr('dy', '.3em')

  const titleLabel = d3.select('#label-title')

  titleLabel
    .append('tspan')
    .attr('class', 'label-title__small-letter')
    .text('$')

  titleLabel
    .append('tspan')
    .text('29,684')

  titleLabel
    .append('tspan')
    .attr('class', 'label-title__small-letter')
    .text('.55')

  chartLabel
    .append('text')
    .text(`${TOTAL_ASSETS} Assets`)
    .attr('class', 'label-subtitle')
    .attr('text-anchor', 'middle')
    .attr('dy', '2em')
    .style('opacity', 0)
    .transition()
    .ease(d3.easeLinear)
    .duration(1000)
    .delay(500)
    .style('opacity', 1)

}

export default createLineChart
