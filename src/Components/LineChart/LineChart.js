import React, { Component } from 'react'
import createLineChart from './LineChartConfigs'
import { ChartArea, ChartTooltip } from './LineChartStyles'

export default class LineChart extends Component {

  componentDidMount () {
    createLineChart()
  }

  render () {
    return (
      <React.Fragment>
        <ChartArea id='line-chart-area'>
          <ChartTooltip id='line-chart-tooltip' />
        </ChartArea>
      </React.Fragment>
    )
  }

}
