import styled from 'styled-components'
import { Colors, Fonts } from '@/Resources/Styles/Theme'

export const SidebarItem = styled.div`
  height: 100%;
  width: 180px;
  position: fixed;
  background-color: rgba(255, 255, 255, 0.1);
  padding: 24px 0;
  border-right: solid 1px rgba(255, 255, 255, 0.1);
`

export const exodusLogo = {
  width: 34,
  height: 34,
  margin: '0 auto'
}

export const txtLogo = {
  width: 90,
  height: 26,
  margin: '13px auto 0'
}

export const AmountBox = styled.div`
  margin-top: 21px;
  text-align: center;
`

export const CurrentAmount = styled.h2`
  font-size: 18px;
  text-align: center;
  font-family: ${Fonts.condensed.fontFamily}

  & span {
    color: ${Colors.gray.color}
  }
`

export const currentCurrency = {
  fontSize: 22,
  ...Colors.white
}
