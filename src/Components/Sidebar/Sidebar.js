import React, { Component } from 'react'
import {
  SidebarItem,
  exodusLogo,
  txtLogo,
  AmountBox,
  CurrentAmount,
  currentCurrency
} from './SidebarStyles'
import { Images } from '@/Resources/Images'
import { Link } from 'react-router-dom'
import { Menu } from '@/Components'

export default class Sidebar extends Component {

  render () {

    return (
      <React.Fragment>
        <SidebarItem>
          <Link to='/'>
            <img src={Images.logo} alt='Exodus Logo' style={exodusLogo} />
            <img src={Images.txtLogo} alt='Exodus' style={txtLogo} />
          </Link>

          <AmountBox>
            <CurrentAmount>
              <span>$</span>
              <span style={currentCurrency}>29,684 </span>
              <span>USD</span>
            </CurrentAmount>
          </AmountBox>
          <Menu />
        </SidebarItem>
      </React.Fragment>
    )
  }

}
