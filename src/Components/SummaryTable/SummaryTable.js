import React, { Component } from 'react'
import { Table } from './SummaryTableStyles'

export default class SummaryTable extends Component {

  render () {
    return (
      <Table>
        <thead>
          <tr>
            <th>24h Change</th>
            <th>Lowest Balance</th>
            <th>Highest Balance</th>
            <th>Market Cap</th>
            <th>Portfolio</th>
            <th>24h Volume</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>+ $2,930.03</td>
            <td>$4,456.50</td>
            <td>$47,839.82</td>
            <td>+ 32.44%</td>
            <td>+ 48.55%</td>
            <td>$47,839.82</td>
          </tr>
        </tbody>
      </Table>
    )
  }

}
