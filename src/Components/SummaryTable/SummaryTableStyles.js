import styled from 'styled-components'
import { Colors } from '@/Resources/Styles/Theme'

export const Table = styled.table`
  width: 100%;
  margin-top: 37px;

  th {
    font-size: 16px;
    color: ${Colors.gray.color};
    border-right: 1px solid rgba(216, 216, 216, 0.05);
    padding: 2px 0;

    &:last-child {
      border-right: none;
    }
  }

  td {
    border-right: 1px solid rgba(216, 216, 216, 0.05);
    text-align: center;
    padding: 2px 0;
    font-size: 18px;

    &:last-child {
      border-right: none;
    }
  }
`
