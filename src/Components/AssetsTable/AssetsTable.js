import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Table, TdItem, PlaceholderTable } from './AssetsTableStyles'
import { Icons } from '@/Resources/Images'

@inject('portfolioStore')
@observer
export default class AssetsTable extends Component {

  componentWillMount () {
    this.props.portfolioStore.loadCryptocurrencies()
  }

  isFetchingList = () => {
    return this.props.portfolioStore.isLoading
  }

  renderAssetsData = () => {
    const { list } = this.props.portfolioStore

    return (
      list.map(coin => {
        const { name, symbol, id, amount } = coin
        const { price, percent_change_24h, volume_24h } = coin.quotes.USD
        const icon = `${Icons.coins[symbol.toLowerCase()]}`
        const currentPrice = `$ ${price.toFixed(2)}`
        const change24h = Math.sign(percent_change_24h) === 1 ? `+${percent_change_24h} %` : `${percent_change_24h} %`

        return (
          <tr key={id}>
            <TdItem
              coin={symbol}
            >
              <div className='assets-info'>
                <img src={icon} alt={`${name} Icon`} className='assets-info__coin-icon' />
                <div className='assets-info__box-coin'>
                  <span className='assets-info__coin-name'>{name}</span>
                  <span className='assets-info__coin-amount'>{currentPrice}</span>
                </div>
              </div>
            </TdItem>
            <TdItem coin={symbol}>img</TdItem>
            <TdItem coin={symbol}>{change24h}</TdItem>
            <TdItem coin={symbol}>{volume_24h}</TdItem>
            {/* TODO */}
            <TdItem coin={symbol}>+ 48.55%</TdItem>
            <TdItem coin={symbol}>{`${amount} ${symbol}`}</TdItem>
            {/* TODO */}
          </tr>
        )
      })
    )
  }

  renderTableData = () => {
    return (
      <Table>
        <thead>
          <tr>
            <th>Asset</th>
            <th>Graph (7d)</th>
            <th>Change (24h)</th>
            <th>Volume(24h)</th>
            <th>Fiat Value</th>
            <th>Digital Value</th>
          </tr>
        </thead>
        <tbody>
          {this.renderAssetsData()}
        </tbody>
      </Table>
    )
  }

  renderPlaceholderTable = () => {
    const lines = 8
    return (
      <PlaceholderTable>
        {[...Array(lines)].map((val, i) => (<div key={i} className='placeholder-table__item'></div>))}
      </PlaceholderTable>
    )
  }

  render () {
    return (
      <React.Fragment>
        {
          !this.isFetchingList()
            ?
            this.renderTableData()
            :
            this.renderPlaceholderTable()
        }
      </React.Fragment>
    )
  }

}
