import styled from 'styled-components'
import { Colors, Fonts } from '@/Resources/Styles/Theme'

export const Table = styled.table`
  width: 100%;
  margin-top: 25px;
  border-collapse: separate;
  border-spacing: 0 22px;

  thead {
    background-color: #2e353b;

    th {
      font-size: 14px;
      color: rgba(237, 237, 237, 0.2);
      padding: 17px 0;

      &:first-child {
        padding-left: 20px;
      }
    }
  }

  tbody {

    tr {

      &:hover {
        td {
          top: -2px;
          opacity: 1;
        }
      }

      td {
        text-align: center;
        font-size: 18px;
        padding: 12px;
        vertical-align: middle;
        border-bottom-width: 2px;
        border-bottom-style: solid;
        background-color: #2f353b;
        position: relative;
        transition: all 300ms ease;
        opacity: 0.8;
        top: 0;

        &:first-child {
          width: 190px;
          text-align: left;
          background-color: #33393f;
        }
      }
    }
  }

  .assets-info {
    display: inline-block;

    &__coin-icon {
      opacity: 0.8;
      width: 40px;
      height: 40px;
      display: inline-block;
      vertical-align: middle;
    }

    &__box-coin {
      display: inline-block;
      margin-left: 12px;
      vertical-align: middle;
      text-align: left;
    }

    &__coin-name {
      color: #c7c8c9;
      font-size: 16px;
      display: block;
      font-family: ${Fonts.condensedBold.fontFamily}
    }

    &__coin-amount {
      display: block;
      color: ${Colors.lightGray.color};
      opacity: ${Colors.lightGray.opacity}
      font-size: 16px;
    }
  }
`

export const TdItem = styled.td`
  border-bottom-color: ${props => Colors.coins[props.coin]};
`

export const PlaceholderTable = styled.div`
  width: 100%;
  margin-top: 35px;

  .placeholder-table__item {
    margin-bottom: 30px;
    animation-duration: 500ms;
    animation-iteration-count: infinite;
    animation-name: placeHolderShimmer;
    animation-timing-function: linear;
    background: linear-gradient(to right, #33393f, #333, #33393f);
    background-size: 800px 100px;
    height: 60px;
  }

  @keyframes placeHolderShimmer{
    from { background-position: 0 0; }
	  to { background-position: 100% 0; }
  }
`
