import React, { Component } from 'react'
import createDonutChart from './DonutChartConfigs'
import { ChartArea, ChartTooltip } from './DonutChartStyles'

export default class DonutGraph extends Component {

  componentDidMount () {
    createDonutChart()
  }

  render () {
    return (
      <React.Fragment>
        <ChartArea id='donut-chart-area'>
          <ChartTooltip id='donut-chart-tooltip' />
        </ChartArea>
      </React.Fragment>
    )
  }

}
