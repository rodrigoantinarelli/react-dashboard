import styled from 'styled-components'
import { Colors } from '@/Resources/Styles/Theme'

export const ChartArea = styled.div`
  text-align: center;
  margin-bottom: 10px;

  .arc {

    path {
      stroke: #262b30;
      stroke-width: 2px;
      cursor: pointer;
    }
  }

  .label-title {
    font-size: 60px;
    letter-spacing: -1.7px;
    fill: ${Colors.white.color};

    &__small-letter {
      font-size: 42px;
    }
  }

  .label-subtitle {
    font-size: 24px;
    letter-spacing: -0.7px;
    fill: ${Colors.gray.color};
  }
`

export const ChartTooltip = styled.div`
  position: absolute;
  text-align: center;
  width: auto;
  height: auto;
  padding: 5px 40px;
  border-radius: 5px;
  pointer-events: none;
  color: white;
  font-size: 20px;
  opacity: 0;
  background-color: #2b3137;
  box-shadow: 0 50px 60px 0 #00000080;
`
