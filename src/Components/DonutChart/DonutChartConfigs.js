import * as d3 from 'd3'
import DATA from '@/Data/coins.json'
import { Colors } from '@/Resources/Styles/Theme'

const COLORS_ARRAY = DATA.map(val => Colors.coins[val.name])
const TOTAL_ASSETS = DATA.length

const width = 410
const height = 410
const radius = width / 2
const color = d3.scaleOrdinal().range(COLORS_ARRAY)

const createDonutChart = () => {

  const arc = d3.arc()
    .outerRadius(radius - 10)
    .innerRadius(radius - 15)

  const pie = d3.pie()
    .sort(null)
    .value((d) => { return d.amount })

  const svg = d3.select('#donut-chart-area').append('svg')
    .attr('width', width)
    .attr('height', height)
    .append('g')
    .attr('transform', 'translate(' + width / 2 + ',' + height / 2 + ')')

  DATA.forEach((d) => {
    d.amount = +d.amount
    d.name = d.name
  })

  const tooltip = d3.select('#donut-chart-tooltip')
    .style('opacity', 0)

  const g = svg.selectAll('.arc')
    .data(pie(DATA))
    .enter().append('g')
    .attr('class', 'arc')
    .on('mouseover', function (d) {
      tooltip
        .style('opacity', 1)
      tooltip.html(d.data.name)
        .style('left', `${d3.event.pageX - 160}px`)
        .style('top', `${d3.event.pageY - 40}px`)
        .style('background-color', Colors.coins[d.data.name])
    })
    .on('mousemove', function (d) {
      tooltip
        .style('opacity', 1)
      tooltip.html(d.data.name)
        .style('left', `${d3.event.pageX - 160}px`)
        .style('top', `${d3.event.pageY - 40}px`)
        .style('background-color', Colors.coins[d.data.name])
    })
    .on('mouseout', function () {
      tooltip
        .style('opacity', 0)
    })

  const animateChart = (value) => {
    const interpolate = d3.interpolate({startAngle: 0, endAngle: 0}, value)
    return function (transition) {
      return arc(interpolate(transition))
    }
  }

  const chartLabel = svg.append('g')
    .attr('class', 'chart-label-group')
    .style('transform', 'translate(' + (width / 2) + ',' + (height / 2) + ')')

  chartLabel
    .append('text')
    .attr('id', 'label-title')
    .attr('class', 'label-title')
    .attr('text-anchor', 'middle')
    .attr('dy', '.8em')
    .style('opacity', 0)
    .transition()
    .ease(d3.easeLinear)
    .duration(500)
    .style('opacity', 1)
    .attr('dy', '.3em')

  const titleLabel = d3.select('#label-title')

  titleLabel
    .append('tspan')
    .attr('class', 'label-title__small-letter')
    .text('$')

  titleLabel
    .append('tspan')
    .text('29,684')

  titleLabel
    .append('tspan')
    .attr('class', 'label-title__small-letter')
    .text('.55')

  chartLabel
    .append('text')
    .text(`${TOTAL_ASSETS} Assets`)
    .attr('class', 'label-subtitle')
    .attr('text-anchor', 'middle')
    .attr('dy', '2em')
    .style('opacity', 0)
    .transition()
    .ease(d3.easeLinear)
    .duration(500)
    .delay(500)
    .style('opacity', 1)

  g.append('path')
    .attr('d', arc)
    .style('fill', d => { return color(d.data.name) })
    .transition()
    .ease(d3.easeCircleOut)
    .duration(500)
    .attrTween('d', animateChart)

}

export default createDonutChart
