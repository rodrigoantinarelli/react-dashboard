import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { FilterWrapper, LabelBox, Line, Bullet, Label } from './FilterDataStyles'
import { inject, observer } from 'mobx-react'
import { action, observable, computed } from 'mobx'

@inject('portfolioStore')
@observer
export default class FilterData extends Component {

  @observable bulletPosition = {}

  @computed get activeTab () {
    return this.props.portfolioStore.filterTab
  }

  @computed get activeTabPosition () {
    return this.bulletPosition
  }

  @action setTabPosition (activeTab) {
    const tabPosition = ReactDOM.findDOMNode(this.refs[activeTab]).getBoundingClientRect()
    this.bulletPosition = tabPosition
  }

  componentDidMount () {
    this.setTabPosition(this.props.activeTab)
  }

  componentWillMount () {
    const { activeTab } = this.props
    this.props.portfolioStore.setActiveFilterTab(activeTab)
  }

  renderTabItem = (label) => {
    return (
      <Label
        ref={label}
        className={this.activeTab === label ? 'active' : ''}
        onClick={() => {
          this.setTabPosition(label)
          this.props.portfolioStore.setActiveFilterTab(label)}
        }
      >
        {label}
      </Label>
    )
  }

  render () {

    return (
      <FilterWrapper>
        <Line />
        <Bullet
        />
        <LabelBox>
          {this.renderTabItem('NOW')}
          {this.renderTabItem('1D')}
          {this.renderTabItem('1W')}
          {this.renderTabItem('1M')}
          {this.renderTabItem('ALL')}
        </LabelBox>
      </FilterWrapper>
    )
  }

}
