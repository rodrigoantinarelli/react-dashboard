import styled from 'styled-components'
// import { Colors } from '@/Resources/Styles/Theme'

export const FilterWrapper = styled.div`
  width: 100%;
  margin-bottom: 37px;
`

export const Line = styled.hr`
  border-color: rgba(255, 255, 255, 0.05);
`

export const Bullet = styled.div`
  position: absolute;
  width: 3px;
  height: 3px;
  background-color: #d8d8d8;
  top: 0;
  transition: all 300ms ease;
  display: none;
`

export const LabelBox = styled.div`
  text-align: center;
`



export const Label = styled.span`
  display: inline-block;
  color: rgba(255, 255, 255, 0.1);
  font-size: 14px;
  margin: 0 25px;
  text-align: center;
  cursor: pointer;
  transition: all 300ms ease;
  position: relative;

  &.active, &:hover {
    color: #fff;
  }
`

