import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Icons } from '@/Resources/Images'
import { MenuBox, MenuBoxBottom, MenuItem, iconMenu } from './MenuStyles'
import { inject, observer } from 'mobx-react'

@inject('appStore')
@observer
export default class Menu extends Component {

  getCurrentPage = () => {
    return this.props.appStore.currentPage.pathname
  }

  renderMenuItem = (to) => {
    const isMenuActive = this.getCurrentPage().split('/').some(val => val === to)

    return(
      <MenuItem className={isMenuActive && 'active'}>
        <Link to={`/${to}`}>
          <img src={Icons[to]} alt='Menu Icon' style={iconMenu} />
          {to}
        </Link>
      </MenuItem>
    )
  }

  render () {

    return (
      <React.Fragment>
        <MenuBox>
          {this.renderMenuItem('portfolio')}
          {this.renderMenuItem('wallet')}
          {this.renderMenuItem('exchange')}
          {this.renderMenuItem('backup')}
        </MenuBox>

        <MenuBoxBottom>
          {this.renderMenuItem('settings')}
          {this.renderMenuItem('help')}
        </MenuBoxBottom>
      </React.Fragment>
    )
  }

}
