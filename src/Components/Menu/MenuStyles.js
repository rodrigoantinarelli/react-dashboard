import styled from 'styled-components'
import { Fonts, Colors, Transitions } from '@/Resources/Styles/Theme'

export const MenuBox = styled.ul`
  margin-top: 15px
`

export const MenuBoxBottom = styled.ul`
  margin-top: 225px
`

export const MenuItem = styled.li`

  &:hover, &.active {
    a, img {
      color: ${Colors.white.color};
      opacity: ${Colors.white.opacity}
    }

    a {
      background: #333b42;
      border-top: solid 1px rgba(255, 255, 255, 0.1);
      border-bottom: solid 1px rgba(255, 255, 255, 0.1);

      &:before {
        background: #333b42;
      }
    }
  }

  img {
    display: inline-block;
    vertical-align: middle;
    transition: ${Transitions.hover.transition}
    color: ${Colors.lightGray.color};
    opacity: ${Colors.lightGray.opacity};
    margin-right: 14px;
  }

  a {
    border-top: 1px solid transparent;
    border-bottom: 1px solid transparent;
    text-transform: capitalize;
    font-size: 15px;
    font-family: ${Fonts.regular.fontFamily}
    transition: ${Transitions.hover.transition}
    color: ${Colors.lightGray.color};
    opacity: ${Colors.lightGray.opacity};
    padding: 19px 32px;
    display: block;
    position: relative;

    &:before {
      content: '';
      position: absolute;
      top: 0;
      right: -1px;
      height: 100%;
      width: 1px;
      background: transparent;
    }
  }
`

export const iconMenu = {
  width: 24,
  height: 24
}
