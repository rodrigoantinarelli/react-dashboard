import styled from 'styled-components'
import { Colors, Fonts } from '@/Resources/Styles/Theme'

export const MessageContainer = styled.div`
  padding-top: 139px;

  .home {
    &__welcome-text {
      font-size: 34px;
      font-weight: 300;
      text-align: center;
      font-family: ${Fonts.condensedLight.fontFamily}
    }

    &__headline-text {
      font-size: 20px;
      text-align: center;
      color: ${Colors.lightBlue.color};
      font-family: ${Fonts.condensedLight.fontFamily}

      a {
        text-decoration: underline;
      }
    }
  }
`
