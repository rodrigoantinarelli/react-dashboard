import React, { Component } from 'react'
import { MessageContainer } from './WelcomeMessageStyles'
import { Link } from 'react-router-dom'

export default class WelcomeMessage extends Component {

  render () {

    return (
      <React.Fragment>
        <MessageContainer>
          <h1 className='home__welcome-text'>Welcome to Exodus</h1>
          <h4 className='home__headline-text'>
            Have you seen your <Link to='/portfolio'>portfolio</Link> today?
          </h4>
        </MessageContainer>
      </React.Fragment>
    )
  }

}
