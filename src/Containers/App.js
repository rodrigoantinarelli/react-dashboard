import React, { Component } from 'react'
import Sidebar from '@/Components/Sidebar/Sidebar'
import { Content, Wrapper } from '@/Resources/Styles/UI'
import renderRoutes from '@/routes'
import { BrowserRouter as Router } from 'react-router-dom'
import LocationListener from '@/Containers/LocationListener'
import { configure } from 'mobx'
import { observer, Provider } from 'mobx-react'
import stores from '@/Stores'

configure({ enforceActions: true })

const STORE = window.store = stores

@observer
export default class App extends Component {

  render () {
    return (
      <Provider {...STORE}>
        <Router>
          <LocationListener>
            <Wrapper>
              <Sidebar />
              <Content>
                {renderRoutes()}
              </Content>
            </Wrapper>
          </LocationListener>
        </Router>
      </Provider>
    )
  }

}
