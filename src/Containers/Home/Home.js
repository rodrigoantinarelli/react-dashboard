import React, { Component } from 'react'
import WelcomeMessage from '@/Components/WelcomeMessage/WelcomeMessage'

export default class Home extends Component {

  render () {

    return (
      <React.Fragment>
        <WelcomeMessage />
      </React.Fragment>
    )
  }

}
