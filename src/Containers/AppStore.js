import { action, observable, computed } from 'mobx'

export default class AppStore {

  @observable currentPage = null

  @action setPage (page) {
    this.currentPage = page
  }

  @computed get page () {
    return this.currentPage
  }

}
