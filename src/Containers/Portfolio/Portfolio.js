import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { FilterData, AssetsTable, DonutChart, LineChart, SummaryTable } from "@/Components"
@inject('portfolioStore')
@observer
export default class Portfolio extends Component {

  renderGraph = () => {
    const { activeFilterTab } = this.props.portfolioStore
    switch (activeFilterTab) {
    case 'NOW':
      return <DonutChart />
    case '1D':
      return <LineChart />
    default:
      return <DonutChart />
    }
  }

  render () {

    return (
      <div>
        <div style={{ height: 410 }}>
          {this.renderGraph()}
        </div>

        <FilterData activeTab='NOW' />

        <SummaryTable />

        <AssetsTable />
      </div>
    )
  }

}
