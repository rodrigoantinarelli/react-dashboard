import { action, observable, computed } from 'mobx'
import coins from '@/Data/coins.json'
import { getCryptoData } from '@/Services/API'
import axios from 'axios'

export default class AppStore {

  @observable cryptoList = []
  @observable loading = true
  @observable filterTab = ''

  @action setActiveFilterTab (tab) {
    this.filterTab = tab
  }

  @computed get activeFilterTab () {
    return this.filterTab
  }

  @action loadCryptocurrencies () {
    if (!this.cryptoList.length > 0) {
      const allRequests = coins.map(coin => {
        const coinData = new getCryptoData(coin.id)
        return coinData
      })

      axios.all(allRequests).then(value => {
        let list = this.filterListByRank(value)

        this.insertCoinAmountOnList(list)

        console.log(list)
        this.insertCrypto(list)
        this.setLoading(false)
      })
    }
  }

  insertCoinAmountOnList = (list) => {
    const newList = list.forEach(item => {
      const currentItemData = coins.filter(coin => item.symbol === coin.name)
      item.amount = currentItemData[0].amount
    })

    return newList
  }

  filterListByRank = (list) => {
    return list.sort((a,b) =>  a['rank'] - b['rank'])
  }

  @action insertCrypto (value) {
    this.cryptoList = value
  }

  @action setLoading (value) {
    this.loading = value
  }

  @computed get isLoading () {
    return this.loading
  }

  @computed get list () {
    return this.cryptoList
  }

}
