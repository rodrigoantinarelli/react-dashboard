import { Component } from 'react'
import PropTypes from 'prop-types'
import { observer, inject } from 'mobx-react'

@inject('appStore')
@observer
class LocationListener extends Component {

  static contextTypes = {
    router: PropTypes.object
  }

  getCurrentLocation = () => {
    return this.context.router.history.location
  }

  componentWillMount () {
    this.setCurrentPage(this.getCurrentLocation())
    this.context.router.history.listen(this.setCurrentPage)
  }

  setCurrentPage = (location) => {
    this.props.appStore.setPage(location)
    this.setState({})
  }

  render () {
    return (
      this.props.children
    )
  }

}

export default LocationListener
