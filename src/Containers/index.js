import Home from './Home/Home'
import Portfolio from './Portfolio/Portfolio'
import NoMatch from './NoMatch/NoMatch'

export {
  Home,
  Portfolio,
  NoMatch
}
