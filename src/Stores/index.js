import AppStore from '@/Containers/AppStore'
import PortfolioStore from '@/Containers/Portfolio/PortfolioStore'

export default {
  appStore: new AppStore(),
  portfolioStore: new PortfolioStore()
}
