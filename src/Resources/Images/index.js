const Images = {
  logo: require('./logo.svg'),
  txtLogo: require('./txt-logo.svg'),
  xBg: require('./x.svg')
}

const Icons = {
  backup: require('./Icons/backup.svg'),
  exchange: require('./Icons/exchange.svg'),
  help: require('./Icons/help.svg'),
  portfolio: require('./Icons/portfolio.svg'),
  settings: require('./Icons/settings.svg'),
  wallet: require('./Icons/wallet.svg'),

  coins: {
    btc: require('./Icons/Coins/btc.svg'),
    dash: require('./Icons/Coins/dash.svg'),
    dcr: require('./Icons/Coins/dcr.svg'),
    dnt: require('./Icons/Coins/dnt.svg'),
    edg: require('./Icons/Coins/edg.svg'),
    eos: require('./Icons/Coins/eos.svg'),
    etc: require('./Icons/Coins/etc.svg'),
    eth: require('./Icons/Coins/eth.svg'),
    ltc: require('./Icons/Coins/ltc.svg'),
    rlc: require('./Icons/Coins/rlc.svg'),
    wings: require('./Icons/Coins/wings.svg')
  }
}

export {
  Images,
  Icons
}
