import styled,{ injectGlobal } from 'styled-components'

import Roboto from '@/Resources/Fonts/Roboto-Regular-webfont.woff'
import RobotoLight from '@/Resources/Fonts/Roboto-Light-webfont.woff'
import RobotoBold from '@/Resources/Fonts/Roboto-Bold-webfont.woff'
import RobotoCondensed from '@/Resources/Fonts/RobotoCondensed-Regular-webfont.woff'
import RobotoCondensedLight from '@/Resources/Fonts/RobotoCondensed-Light-webfont.woff'
import RobotoCondensedBold from '@/Resources/Fonts/RobotoCondensed-Bold-webfont.woff'

injectGlobal`
  *,:after,:before{box-sizing:inherit}html{box-sizing:border-box}
  *{-webkit-tap-highlight-color:rgba(255,255,255,0);-webkit-tap-highlight-color:transparent}
  html,body,div,span,applet,object,iframe,h1,h2,h3,h4,h5,h6,p,blockquote,
  pre,a,abbr,acronym,address,big,cite,code,del,dfn,em,img,ins,kbd,q,s,samp,
  small,strike,strong,sub,sup,tt,var,b,u,i,center,dl,dt,dd,ol,ul,li,fieldset,form,
  label,legend,table,caption,tbody,tfoot,thead,tr,th,td,article,aside,canvas,details,
  embed,figure,figcaption,footer,header,hgroup,menu,nav,output,ruby,section,summary,time,
  mark,audio,video{margin:0;padding:0;border:0;font-size:100%;font:inherit;vertical-align:baseline}
  article,aside,details,figcaption,figure,footer,header,hgroup,menu,nav,section{display:block}
  ol,ul{list-style:none}
  blockquote,q{quotes:none}
  blockquote:before,blockquote:after,q:before,q:after{content:'';content:none}
  table{border-collapse:collapse;border-spacing:0}
  [contenteditable],input[type]{user-select:text}
  a{text-decoration:none;color:inherit}
  fieldset,input{appearance:none;border:0;padding:0;margin:0;min-width:0;font-size:1rem;font-family:inherit}
  input[type=number]::-webkit-inner-spin-button,input[type=number]::-webkit-outer-spin-button{appearance:none}
  svg{display:inline-flex}
  img{display:block;width:100%}
  body,h1,h2,h3,h4,h5,h6,p{margin:0;font-size:1rem;font-weight:400}

  @font-face{
    font-family: "Roboto";
    src: url(${Roboto}) format("woff2"),
    url(${Roboto}) format("woff");
    font-style: 'normal';
    font-weight: 'normal';
  }

  @font-face{
    font-family: "RobotoLight";
    src: url(${RobotoLight}) format("woff2"),
    url(${RobotoLight}) format("woff");
    font-style: 'normal';
    font-weight: 'normal';
  }

  @font-face{
    font-family: "RobotoBold";
    src: url(${RobotoBold}) format("woff2"),
    url(${RobotoBold}) format("woff");
    font-style: 'normal';
    font-weight: 'normal';
  }

  @font-face{
    font-family: "RobotoCondensedLight";
    src: url(${RobotoCondensedLight}) format("woff2"),
    url(${RobotoCondensedLight}) format("woff");
    font-style: 'normal';
    font-weight: 'normal';
  }

  @font-face{
    font-family: "RobotoCondensed";
    src: url(${RobotoCondensed}) format("woff2"),
    url(${RobotoCondensed}) format("woff");
    font-style: 'normal';
    font-weight: 'normal';
  }

  @font-face{
    font-family: "RobotoCondensedBold";
    src: url(${RobotoCondensedBold}) format("woff2"),
    url(${RobotoCondensedBold}) format("woff");
    font-style: 'normal';
    font-weight: 'normal';
  }

  body {
    margin: 0;
    padding: 0;
    font-family: "RobotoCondensedLight";
    height: 100%;
    color: white;
    background-image: linear-gradient(172deg, #333b42, #181a1b);
    background-attachment: fixed;
  }

  html {
    height: 100%
  }
`

export const Wrapper = styled.div`
  width: 100%;
`

export const Content = styled.div`
    height: 100%;
    padding: 58px 80px;
    margin-left: 180px;
    width: calc(100% - 180px);
    position: relative;
`
