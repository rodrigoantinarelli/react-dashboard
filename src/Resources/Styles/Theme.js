const Colors = {
  white: {
    color: '#fafafa',
    opacity: 1
  },
  lightGray: {
    color: '#d1d1d1',
    opacity: 0.8
  },
  gray: {
    color: 'rgba(250, 250, 250, 0.5)'
  },
  lightBlue: {
    color: '#46d6fc'
  },
  coins: {
    BTC: '#c48338',
    DASH: '#3765a2',
    ETH: '#626baa',
    DCR: '#4b919f',
    ETC: '#588a68',
    EOS: '#576f9d',
    LTC: '#898b8b',
    EDG: '#6c60a0',
    WINGS: '#6db0c9',
    RLC: '#cdb53d',
    DNT: '#4a89a8'
  }
}

const Fonts = {
  regular: {
    fontFamily: 'Roboto'
  },
  light: {
    fontFamily: 'RobotoLight'
  },
  bold: {
    fontFamily: 'RobotoBold'
  },
  condensed: {
    fontFamily: 'RobotoCondensed'
  },
  condensedLight: {
    fontFamily: 'RobotoCondensedLight'
  },
  condensedBold: {
    fontFamily: 'RobotoCondensedBold'
  }
}

const Transitions = {
  hover: {
    transition: 'all 300ms ease'
  }
}

export {
  Colors,
  Fonts,
  Transitions
}
